python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm in
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm brn
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm ln
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bin
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bn
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm tn
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm sn

python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm in --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm brn --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm ln --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bin --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bn --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm tn --with-davies
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm sn --with-davies

python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm in --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm brn --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm ln --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bin --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bn --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm tn --davies-only
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm sn --davies-only

python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm in --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm brn --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm ln --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bin --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm bn --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm tn --attention
python -m experiments.proto_nets --dataset omniglot --k-test 5 --n-test 1 --norm sn --attention


python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm in
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm brn
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm ln
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bin
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bn
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm tn
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm sn

python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm in --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm brn --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm ln --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bin --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bn --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm tn --with-davies
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm sn --with-davies

python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm in --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm brn --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm ln --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bin --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bn --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm tn --davies-only
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm sn --davies-only

python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm in --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm brn --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm ln --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bin --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm bn --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm tn --attention
python -m experiments.proto_nets --dataset miniImageNet --k-test 5 --n-test 1 --k-train 20 --n-train 1 --q-train 15 --norm sn --attention

