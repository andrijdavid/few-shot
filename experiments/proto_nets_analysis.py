from few_shot.datasets import OmniglotDataset, MiniImageNet
from few_shot.models import get_few_shot_encoder
from few_shot.core import NShotTaskSampler, EvaluateFewShot, prepare_nshot_task
from few_shot.proto import proto_net_episode
from few_shot.train import fit
from few_shot.callbacks import *
from few_shot.utils import setup_dirs
from config import PATH
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--weight')
args = parser.parse_args()

model = get_few_shot_encoder(num_input_channels, norm=args.norm)
filepath= PATH + f'/models/proto_nets/{args.weight}'
model.load_state_dict(torch.load(filepath))
model.eval()