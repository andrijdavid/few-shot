from few_shot.datasets import OmniglotDataset as OrigOmniglotDataset, MiniImageNet as OrigMiniImageNet
from torchvision import transforms
from norse.torch import ConstantCurrentLIFEncoder, PoissonEncoder, SignedPoissonEncoder
from norse.torch import LIFParameters
import torch

def add_luminance(images):
    return torch.cat(
        (
            images,
            torch.unsqueeze(
                0.2126 * images[0, :, :]
                + 0.7152 * images[1, :, :]
                + 0.0722 * images[2, :, :],
                0,
            ),
        ),
        0,
    )
    
SEQ_LENGTH = 10

constant_current_encoder = ConstantCurrentLIFEncoder(
    seq_length=SEQ_LENGTH, p=LIFParameters(v_th=0.7)
)

def polar_current_encoder(x):
    x_p = constant_current_encoder(2 * torch.nn.functional.relu(x))
    x_m = constant_current_encoder(2 * torch.nn.functional.relu(-x))
    return torch.cat((x_p, x_m), 1)

def current_encoder(x):
    x = constant_current_encoder(2 * x)
    return x

def signed_current_encoder(x):
    z = constant_current_encoder(torch.abs(x))
    return torch.sign(x) * z

class OmniglotDataset(OrigOmniglotDataset):
    def __init__(self, subset):
        super().__init__(subset)
        encoder = SignedPoissonEncoder(seq_length=SEQ_LENGTH, f_max=200)
        self.btransform = transforms.Compose([
            # add_luminance,
            encoder
        ])

    def __getitem__(self, index):
        instance, label = super().__getitem__(index)
        return self.btransform(instance), label

class MiniImageNet(OrigMiniImageNet):
    def __init__(self, subset):
        super().__init__(subset)
        encoder = SignedPoissonEncoder(seq_length=SEQ_LENGTH, f_max=10)
        self.btransform = transforms.Compose([
            # add_luminance,
            encoder
        ])

    def __getitem__(self, index):
        instance, label = super().__getitem__(index)
        return self.btransform(instance), label