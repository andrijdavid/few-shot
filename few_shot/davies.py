import torch

def dist_inter_cluster(prototypes):
    """Computes the distance of center points of two clusters.
    The distance inter cluster is the distance in between the prototypes.

    Args:
        Prototypes of shape bs, n_x, d
    Returns:
        A tensor of distance of shape bs, Number of permutation
    """
    return torch.stack([torch.nn.functional.pdist(prototypes[i], p=2) for i in range(prototypes.shape[0])])

def avg_dist_intra_cluster(clusters, prototypes):
    """Computes average intra-cluster distance
    Args:
        clusters of shape bs, n_cluster, n_queries, dimension
        prototypes of shape bs, n_cluster, dimension
    Returns:
        Intra-cluster distance with shape n_cluster, number of permutation of queries
    """
    bs, n_cluster = clusters.shape[0], clusters.shape[1]
    dist = torch.stack([torch.stack([torch.cdist(prototypes[j][i].unsqueeze(0), clusters[j][i], p=2) for i in range(n_cluster)]).squeeze() for j in range(bs)])
    if len(dist.shape) < 3:
        # When the last dimension is one it get discarded somehow we need to put it pack. 
        dist = dist[:,:,None]
    return dist.mean(dim=-1)

def davies_bouldin(clusters, prototypes):
    bs, n_cluster = clusters.shape[0], clusters.shape[1]
    di = dist_inter_cluster(prototypes)
    si = avg_dist_intra_cluster(clusters, prototypes)
    rij = torch.stack([torch.combinations(si[b]).sum(dim=1)/di[b] for b in range(bs)])
    rij_max, input_indexes = rij.max(dim=1)
    return rij_max/n_cluster