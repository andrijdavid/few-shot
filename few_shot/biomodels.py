from torch.nn import functional as F
from torch import nn
from torch.hub import load_state_dict_from_url
from norse.torch.module.lif import LIFFeedForwardLayer
from norse.torch.module.lift import Lift
from norse.torch.module.leaky_integrator import LICell
from norse.torch.module.lif import LIFFeedForwardCell
from norse.torch.functional.lif import LIFParameters
import torch

class Flatten(nn.Module):
    """Converts N-dimensional Tensor of shape [batch_size, d1, d2, ..., dn] to 2-dimensional Tensor
    of shape [batch_size, d1*d2*...*dn].

    # Arguments
        input: Input tensor
    """
    def forward(self, input):
        return input.view(input.size(0), -1)


class ConvBlock(nn.Sequential):
    def __init__(
        self, in_planes, out_planes, norm_layer, kernel_size=3, stride=1
    ):
        padding = (kernel_size - 1) // 2

        super(ConvBlock, self).__init__(
            Lift(
                nn.Conv2d(
                    in_planes,
                    out_planes,
                    kernel_size,
                    stride,
                    padding,
                )
            ),
            Lift(norm_layer(out_planes)),
            Lift(nn.ReLU()),
            # LIFFeedForwardLayer(),
            Lift(nn.MaxPool2d(kernel_size=2, stride=2))
        )

class ConvNet(torch.nn.Module):
    def __init__(
        self, num_channels=1, feature_size=28, method="super", dtype=torch.float
    ):
        super(ConvNet, self).__init__()
        self.features = int(((feature_size - 4) / 2 - 4) / 2)
        self.conv1 = torch.nn.Conv2d(num_channels, 20, 5, 1)
        self.conv2 = torch.nn.Conv2d(20, 50, 5, 1)
        self.fc1 = torch.nn.Linear(self.features * self.features * 50, 500)
        # self.out = LICell(500, 10)
        self.lif0 = LIFFeedForwardCell(
            p=LIFParameters(method=method, alpha=100.0),
        )
        self.lif1 = LIFFeedForwardCell(
            p=LIFParameters(method=method, alpha=100.0),
        )
        # self.lif2 = LIFFeedForwardCell(p=LIFParameters(method=method, alpha=100.0))
        self.dtype = dtype

    def forward(self, x):
        seq_length = x.shape[0]
        batch_size = x.shape[1]

        # specify the initial states
        s0 = None
        s1 = None
        s2 = None
        so = None

        voltages = torch.zeros(
            seq_length, batch_size, 10, device=x.device, dtype=self.dtype
        )

        for ts in range(seq_length):
            z = self.conv1(x[ts, :])
            z, s0 = self.lif0(z, s0)
            z = torch.nn.functional.max_pool2d(z, 2, 2)
            z = 10 * self.conv2(z)
            z, s1 = self.lif1(z, s1)
            z = torch.nn.functional.max_pool2d(z, 2, 2)
            z = z.view(-1, self.features ** 2 * 50)
            z = self.fc1(z)
            z, s2 = self.lif2(z, s2)
            v, so = self.out(torch.nn.functional.relu(z), so)
            voltages[ts, :, :] = v
        return voltages

class BioContainer(nn.Module):
    def __init__(self, model, flatten=True, pool=True):
        super().__init__()
        self.model = model
        self.pool = Lift(nn.AdaptiveAvgPool2d(1)) if pool else nn.Identity()
        self.flatten = Flatten() if flatten else nn.Identity()
    
    def forward(self, input):
        x = self.model(input)
        x = self.pool(x)
        x = self.flatten(x)
        return x

def conv_block(in_channels: int, out_channels: int, norm:nn.Module = nn.BatchNorm2d) -> nn.Module:
    """Returns a Module that performs 3x3 convolution, ReLu activation, 2x2 max pooling.

    # Arguments
        in_channels:
        out_channels:
    """
    return nn.Sequential(
        nn.Conv2d(in_channels, out_channels, 3, padding=1),
        norm(out_channels),
        nn.ReLU(),
        nn.MaxPool2d(kernel_size=2, stride=2)
    )

def get_few_shot_encoder_s(num_input_channels=1, norm='bn') -> nn.Module:
    """Creates a few shot encoder as used in Matching and Prototypical Networks

    # Arguments:
        num_input_channels: Number of color channels the model expects input data to contain. Omniglot = 1,
            miniImageNet = 3
    """
    n = nn.BatchNorm2d
    return nn.Sequential(
        conv_block(num_input_channels, 64, norm=n),
        conv_block(64, 64, norm=n),
        conv_block(64, 64, norm=n),
        conv_block(64, 64, norm=n),
        # Flatten(),
    )

def get_few_shot_encoder(num_input_channels=1, norm='bn') -> nn.Module:
    """Creates a few shot encoder as used in Matching and Prototypical Networks

    # Arguments:
        num_input_channels: Number of color channels the model expects input data to contain. Omniglot = 1,
            miniImageNet = 3
    """
    n = nn.BatchNorm2d
    model = nn.Sequential(
        ConvBlock(num_input_channels, 64, norm_layer=n),
        ConvBlock(64, 64, norm_layer=n),
        ConvBlock(64, 64, norm_layer=n),
        ConvBlock(64, 64, norm_layer=n)
    )
    return nn.DataParallel(BioContainer(model, flatten=True, pool=False))
    # return  nn.Sequential(ConvNet(num_input_channels), Flatten())
    # return nn.DataParallel(BioContainer(model, flatten=True, pool=False))